start:
	python app.python

lint:
	echo 'lint'

.PHONY: tests
tests:
	python -m pytest tests/t.py

setup:
	pip install virtualenv
	virtualenv venv
	source venv/bin/activate
	pip install -r eq.txt
